import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductsService } from '../../services/products.service';
import { ProductDesciption } from '../../interfaces/product-description.interface';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {

  product: ProductDesciption;
  id: string;

  constructor(
    private route: ActivatedRoute,
    public _productsService: ProductsService
  ) { }

  ngOnInit() {

    this.route.params.subscribe( params => {
      // console.log(params['id']);
      this._productsService.getProduct(params['id'])
        .subscribe( (product: ProductDesciption) => {
          this.id = params['id'];
          this.product = product;
        });
    });
  }

}
