import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { InfoPagina } from '../interfaces/info-pagina.interface';


@Injectable()
export class InfoPaginaService {

  info: InfoPagina = {};
  loading = false;
  team: any[] = [];

  constructor( private _http: HttpClient ) {
    this.getInfo();
    this.getTeam();
  }


  private getInfo() {
    this._http.get('assets/data/data-pagina.json').subscribe( (response: InfoPagina) => {
      this.info = response;
      this.loading = true;
    });
  }


  // Get Firebase data
  private getTeam() {
    this._http.get('https://app-simple-template.firebaseio.com/team.json').subscribe( (response: any) => {
      this.team = response;
      this.loading = true;
    });

  }

}
