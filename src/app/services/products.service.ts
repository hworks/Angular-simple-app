import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Product } from '../interfaces/product.interface';

@Injectable()
export class ProductsService {

  loading = true;
  products: Product[] = [];
  searchProducts: Product[] = [];

  constructor( private _http: HttpClient ) {
    this.loadProducts();
  }

  // ##############################################################################

  private loadProducts() {

    return new Promise( ( resolve, reject ) => {
      this._http.get('https://app-simple-template.firebaseio.com/products_idx.json')
        .subscribe( (response: Product[]) => {
          this.products = response;
          this.loading = false;
          resolve();
      });
    });

  }

  // ###############################################################################

  public getProduct( id: string ) {
    return this._http.get(`https://app-simple-template.firebaseio.com/products/${ id }.json`);
  }

  // ###############################################################################

  public searchProduct( param: string ) {

    if ( this.products.length === 0 ) {
      this.loadProducts().then( () => {
        this.filterProducts( param ); // Después de tener los productos, aplica filtro
      });
    } else {
      this.filterProducts( param );
    }

  }

  // ##############################################################################

  private filterProducts( param: string ) {

    this.searchProducts = []; // Vacía array

    param = param.toLocaleLowerCase();

    this.products.forEach( prod => {

      const tituloLower = prod.titulo.toLocaleLowerCase();

      if ( prod.categoria.indexOf( param ) >= 0 || tituloLower.indexOf( param ) >= 0 ) {
        this.searchProducts.push( prod );
      }

    });

  }

}
